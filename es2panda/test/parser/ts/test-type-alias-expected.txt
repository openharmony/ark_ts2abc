{
  "type": "Program",
  "statements": [
    {
      "type": "TSTypeAliasDeclaration",
      "id": {
        "type": "Identifier",
        "name": "foo",
        "decorators": [],
        "loc": {
          "start": {
            "line": 1,
            "column": 6
          },
          "end": {
            "line": 1,
            "column": 9
          }
        }
      },
      "typeAnnotation": {
        "type": "TSUnionType",
        "types": [
          {
            "type": "TSNumberKeyword",
            "loc": {
              "start": {
                "line": 1,
                "column": 12
              },
              "end": {
                "line": 1,
                "column": 18
              }
            }
          },
          {
            "type": "TSStringKeyword",
            "loc": {
              "start": {
                "line": 1,
                "column": 21
              },
              "end": {
                "line": 1,
                "column": 27
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 12
          },
          "end": {
            "line": 1,
            "column": 27
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 28
        }
      }
    },
    {
      "type": "EmptyStatement",
      "loc": {
        "start": {
          "line": 1,
          "column": 27
        },
        "end": {
          "line": 1,
          "column": 28
        }
      }
    },
    {
      "type": "TSTypeAliasDeclaration",
      "id": {
        "type": "Identifier",
        "name": "bar",
        "decorators": [],
        "loc": {
          "start": {
            "line": 2,
            "column": 6
          },
          "end": {
            "line": 2,
            "column": 9
          }
        }
      },
      "typeAnnotation": {
        "type": "TSUnionType",
        "types": [
          {
            "type": "TSTypeReference",
            "typeName": {
              "type": "Identifier",
              "name": "foo",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 2,
                  "column": 12
                },
                "end": {
                  "line": 2,
                  "column": 15
                }
              }
            },
            "loc": {
              "start": {
                "line": 2,
                "column": 12
              },
              "end": {
                "line": 2,
                "column": 15
              }
            }
          },
          {
            "type": "TSParenthesizedType",
            "typeAnnotation": {
              "type": "TSFunctionType",
              "params": [
                {
                  "type": "Identifier",
                  "name": "a",
                  "typeAnnotation": {
                    "type": "TSNumberKeyword",
                    "loc": {
                      "start": {
                        "line": 2,
                        "column": 23
                      },
                      "end": {
                        "line": 2,
                        "column": 29
                      }
                    }
                  },
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 20
                    },
                    "end": {
                      "line": 2,
                      "column": 21
                    }
                  }
                },
                {
                  "type": "Identifier",
                  "name": "b",
                  "typeAnnotation": {
                    "type": "TSStringKeyword",
                    "loc": {
                      "start": {
                        "line": 2,
                        "column": 34
                      },
                      "end": {
                        "line": 2,
                        "column": 40
                      }
                    }
                  },
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 31
                    },
                    "end": {
                      "line": 2,
                      "column": 32
                    }
                  }
                }
              ],
              "returnType": {
                "type": "TSVoidKeyword",
                "loc": {
                  "start": {
                    "line": 2,
                    "column": 45
                  },
                  "end": {
                    "line": 2,
                    "column": 49
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 2,
                  "column": 19
                },
                "end": {
                  "line": 2,
                  "column": 49
                }
              }
            },
            "loc": {
              "start": {
                "line": 2,
                "column": 18
              },
              "end": {
                "line": 2,
                "column": 50
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 2,
            "column": 12
          },
          "end": {
            "line": 2,
            "column": 50
          }
        }
      },
      "loc": {
        "start": {
          "line": 2,
          "column": 1
        },
        "end": {
          "line": 2,
          "column": 51
        }
      }
    },
    {
      "type": "EmptyStatement",
      "loc": {
        "start": {
          "line": 2,
          "column": 50
        },
        "end": {
          "line": 2,
          "column": 51
        }
      }
    },
    {
      "type": "TSTypeAliasDeclaration",
      "id": {
        "type": "Identifier",
        "name": "baz",
        "decorators": [],
        "loc": {
          "start": {
            "line": 3,
            "column": 6
          },
          "end": {
            "line": 3,
            "column": 9
          }
        }
      },
      "typeAnnotation": {
        "type": "TSUnionType",
        "types": [
          {
            "type": "TSTypeReference",
            "typeName": {
              "type": "Identifier",
              "name": "foo",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 3,
                  "column": 12
                },
                "end": {
                  "line": 3,
                  "column": 15
                }
              }
            },
            "loc": {
              "start": {
                "line": 3,
                "column": 12
              },
              "end": {
                "line": 3,
                "column": 15
              }
            }
          },
          {
            "type": "TSArrayType",
            "elementType": {
              "type": "TSTypeReference",
              "typeName": {
                "type": "Identifier",
                "name": "bar",
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 3,
                    "column": 18
                  },
                  "end": {
                    "line": 3,
                    "column": 21
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 3,
                  "column": 18
                },
                "end": {
                  "line": 3,
                  "column": 21
                }
              }
            },
            "loc": {
              "start": {
                "line": 3,
                "column": 18
              },
              "end": {
                "line": 3,
                "column": 23
              }
            }
          },
          {
            "type": "TSArrayType",
            "elementType": {
              "type": "TSNumberKeyword",
              "loc": {
                "start": {
                  "line": 3,
                  "column": 26
                },
                "end": {
                  "line": 3,
                  "column": 32
                }
              }
            },
            "loc": {
              "start": {
                "line": 3,
                "column": 26
              },
              "end": {
                "line": 3,
                "column": 34
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 3,
            "column": 12
          },
          "end": {
            "line": 3,
            "column": 34
          }
        }
      },
      "loc": {
        "start": {
          "line": 3,
          "column": 1
        },
        "end": {
          "line": 3,
          "column": 35
        }
      }
    },
    {
      "type": "EmptyStatement",
      "loc": {
        "start": {
          "line": 3,
          "column": 34
        },
        "end": {
          "line": 3,
          "column": 35
        }
      }
    },
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "a",
            "typeAnnotation": {
              "type": "TSTypeReference",
              "typeName": {
                "type": "Identifier",
                "name": "baz",
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 4,
                    "column": 8
                  },
                  "end": {
                    "line": 4,
                    "column": 11
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 4,
                  "column": 8
                },
                "end": {
                  "line": 4,
                  "column": 11
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 4,
                "column": 5
              },
              "end": {
                "line": 4,
                "column": 6
              }
            }
          },
          "init": null,
          "loc": {
            "start": {
              "line": 4,
              "column": 5
            },
            "end": {
              "line": 4,
              "column": 6
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 4,
          "column": 1
        },
        "end": {
          "line": 4,
          "column": 12
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 4,
      "column": 12
    }
  }
}
