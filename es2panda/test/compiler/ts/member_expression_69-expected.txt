{
  "type": "Program",
  "statements": [
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "a",
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 5
              },
              "end": {
                "line": 1,
                "column": 6
              }
            }
          },
          "init": {
            "type": "ArrayExpression",
            "elements": [
              {
                "type": "NumberLiteral",
                "value": 0,
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 10
                  },
                  "end": {
                    "line": 1,
                    "column": 11
                  }
                }
              },
              {
                "type": "NumberLiteral",
                "value": 1,
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 13
                  },
                  "end": {
                    "line": 1,
                    "column": 14
                  }
                }
              },
              {
                "type": "NumberLiteral",
                "value": 2,
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 16
                  },
                  "end": {
                    "line": 1,
                    "column": 17
                  }
                }
              },
              {
                "type": "NumberLiteral",
                "value": 3,
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 19
                  },
                  "end": {
                    "line": 1,
                    "column": 20
                  }
                }
              },
              {
                "type": "NumberLiteral",
                "value": 4,
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 22
                  },
                  "end": {
                    "line": 1,
                    "column": 23
                  }
                }
              }
            ],
            "loc": {
              "start": {
                "line": 1,
                "column": 9
              },
              "end": {
                "line": 1,
                "column": 24
              }
            }
          },
          "loc": {
            "start": {
              "line": 1,
              "column": 5
            },
            "end": {
              "line": 1,
              "column": 24
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 25
        }
      }
    },
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "b",
            "typeAnnotation": {
              "type": "TSNumberKeyword",
              "loc": {
                "start": {
                  "line": 2,
                  "column": 8
                },
                "end": {
                  "line": 2,
                  "column": 14
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 2,
                "column": 5
              },
              "end": {
                "line": 2,
                "column": 6
              }
            }
          },
          "init": null,
          "loc": {
            "start": {
              "line": 2,
              "column": 5
            },
            "end": {
              "line": 2,
              "column": 6
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 2,
          "column": 1
        },
        "end": {
          "line": 2,
          "column": 15
        }
      }
    },
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "c",
            "typeAnnotation": {
              "type": "TSBooleanKeyword",
              "loc": {
                "start": {
                  "line": 3,
                  "column": 8
                },
                "end": {
                  "line": 3,
                  "column": 15
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 3,
                "column": 5
              },
              "end": {
                "line": 3,
                "column": 6
              }
            }
          },
          "init": null,
          "loc": {
            "start": {
              "line": 3,
              "column": 5
            },
            "end": {
              "line": 3,
              "column": 6
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 3,
          "column": 1
        },
        "end": {
          "line": 3,
          "column": 16
        }
      }
    },
    {
      "type": "ExpressionStatement",
      "expression": {
        "type": "AssignmentExpression",
        "operator": "=",
        "left": {
          "type": "Identifier",
          "name": "b",
          "decorators": [],
          "loc": {
            "start": {
              "line": 4,
              "column": 1
            },
            "end": {
              "line": 4,
              "column": 2
            }
          }
        },
        "right": {
          "type": "ChainExpression",
          "expression": {
            "type": "MemberExpression",
            "object": {
              "type": "Identifier",
              "name": "a",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 4,
                  "column": 5
                },
                "end": {
                  "line": 4,
                  "column": 6
                }
              }
            },
            "property": {
              "type": "AssignmentExpression",
              "operator": "=",
              "left": {
                "type": "Identifier",
                "name": "c",
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 4,
                    "column": 9
                  },
                  "end": {
                    "line": 4,
                    "column": 10
                  }
                }
              },
              "right": {
                "type": "NumberLiteral",
                "value": 5,
                "loc": {
                  "start": {
                    "line": 4,
                    "column": 13
                  },
                  "end": {
                    "line": 4,
                    "column": 14
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 4,
                  "column": 9
                },
                "end": {
                  "line": 4,
                  "column": 14
                }
              }
            },
            "computed": true,
            "optional": true,
            "loc": {
              "start": {
                "line": 4,
                "column": 5
              },
              "end": {
                "line": 4,
                "column": 15
              }
            }
          },
          "loc": {
            "start": {
              "line": 4,
              "column": 5
            },
            "end": {
              "line": 4,
              "column": 15
            }
          }
        },
        "loc": {
          "start": {
            "line": 4,
            "column": 1
          },
          "end": {
            "line": 4,
            "column": 15
          }
        }
      },
      "loc": {
        "start": {
          "line": 4,
          "column": 1
        },
        "end": {
          "line": 4,
          "column": 16
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 5,
      "column": 1
    }
  }
}
TypeError: Type '5' is not assignable to type 'boolean'. [member_expression_69.ts:4:9]
