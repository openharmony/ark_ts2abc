{
  "type": "Program",
  "statements": [
    {
      "type": "FunctionDeclaration",
      "function": {
        "type": "ScriptFunction",
        "id": {
          "type": "Identifier",
          "name": "foo",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 10
            },
            "end": {
              "line": 1,
              "column": 13
            }
          }
        },
        "generator": false,
        "async": false,
        "expression": false,
        "params": [
          {
            "type": "AssignmentPattern",
            "left": {
              "type": "ArrayPattern",
              "elements": [
                {
                  "type": "AssignmentPattern",
                  "left": {
                    "type": "Identifier",
                    "name": "a",
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 15
                      },
                      "end": {
                        "line": 1,
                        "column": 16
                      }
                    }
                  },
                  "right": {
                    "type": "NumberLiteral",
                    "value": 2,
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 19
                      },
                      "end": {
                        "line": 1,
                        "column": 20
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 15
                    },
                    "end": {
                      "line": 1,
                      "column": 20
                    }
                  }
                },
                {
                  "type": "ObjectPattern",
                  "properties": [
                    {
                      "type": "Property",
                      "method": false,
                      "shorthand": false,
                      "computed": false,
                      "key": {
                        "type": "Identifier",
                        "name": "b",
                        "decorators": [],
                        "loc": {
                          "start": {
                            "line": 1,
                            "column": 24
                          },
                          "end": {
                            "line": 1,
                            "column": 25
                          }
                        }
                      },
                      "value": {
                        "type": "AssignmentPattern",
                        "left": {
                          "type": "ArrayPattern",
                          "elements": [
                            {
                              "type": "AssignmentPattern",
                              "left": {
                                "type": "Identifier",
                                "name": "c",
                                "decorators": [],
                                "loc": {
                                  "start": {
                                    "line": 1,
                                    "column": 28
                                  },
                                  "end": {
                                    "line": 1,
                                    "column": 29
                                  }
                                }
                              },
                              "right": {
                                "type": "NumberLiteral",
                                "value": 6,
                                "loc": {
                                  "start": {
                                    "line": 1,
                                    "column": 32
                                  },
                                  "end": {
                                    "line": 1,
                                    "column": 33
                                  }
                                }
                              },
                              "loc": {
                                "start": {
                                  "line": 1,
                                  "column": 28
                                },
                                "end": {
                                  "line": 1,
                                  "column": 33
                                }
                              }
                            },
                            {
                              "type": "Identifier",
                              "name": "d",
                              "decorators": [],
                              "loc": {
                                "start": {
                                  "line": 1,
                                  "column": 35
                                },
                                "end": {
                                  "line": 1,
                                  "column": 36
                                }
                              }
                            }
                          ],
                          "loc": {
                            "start": {
                              "line": 1,
                              "column": 27
                            },
                            "end": {
                              "line": 1,
                              "column": 37
                            }
                          }
                        },
                        "right": {
                          "type": "ArrayExpression",
                          "elements": [
                            {
                              "type": "BooleanLiteral",
                              "value": true,
                              "loc": {
                                "start": {
                                  "line": 1,
                                  "column": 41
                                },
                                "end": {
                                  "line": 1,
                                  "column": 45
                                }
                              }
                            },
                            {
                              "type": "ArrayExpression",
                              "elements": [
                                {
                                  "type": "BigIntLiteral",
                                  "value": "",
                                  "bigint": "",
                                  "loc": {
                                    "start": {
                                      "line": 1,
                                      "column": 48
                                    },
                                    "end": {
                                      "line": 1,
                                      "column": 50
                                    }
                                  }
                                },
                                {
                                  "type": "StringLiteral",
                                  "value": "",
                                  "loc": {
                                    "start": {
                                      "line": 1,
                                      "column": 52
                                    },
                                    "end": {
                                      "line": 1,
                                      "column": 57
                                    }
                                  }
                                }
                              ],
                              "loc": {
                                "start": {
                                  "line": 1,
                                  "column": 47
                                },
                                "end": {
                                  "line": 1,
                                  "column": 58
                                }
                              }
                            }
                          ],
                          "loc": {
                            "start": {
                              "line": 1,
                              "column": 40
                            },
                            "end": {
                              "line": 1,
                              "column": 59
                            }
                          }
                        },
                        "loc": {
                          "start": {
                            "line": 1,
                            "column": 27
                          },
                          "end": {
                            "line": 1,
                            "column": 59
                          }
                        }
                      },
                      "kind": "init",
                      "loc": {
                        "start": {
                          "line": 1,
                          "column": 24
                        },
                        "end": {
                          "line": 1,
                          "column": 59
                        }
                      }
                    },
                    {
                      "type": "Property",
                      "method": false,
                      "shorthand": true,
                      "computed": false,
                      "key": {
                        "type": "Identifier",
                        "name": "t",
                        "decorators": [],
                        "loc": {
                          "start": {
                            "line": 1,
                            "column": 61
                          },
                          "end": {
                            "line": 1,
                            "column": 62
                          }
                        }
                      },
                      "value": {
                        "type": "AssignmentPattern",
                        "left": {
                          "type": "Identifier",
                          "name": "t",
                          "decorators": [],
                          "loc": {
                            "start": {
                              "line": 1,
                              "column": 61
                            },
                            "end": {
                              "line": 1,
                              "column": 62
                            }
                          }
                        },
                        "right": {
                          "type": "ObjectExpression",
                          "properties": [
                            {
                              "type": "Property",
                              "method": false,
                              "shorthand": false,
                              "computed": false,
                              "key": {
                                "type": "Identifier",
                                "name": "a",
                                "decorators": [],
                                "loc": {
                                  "start": {
                                    "line": 1,
                                    "column": 67
                                  },
                                  "end": {
                                    "line": 1,
                                    "column": 68
                                  }
                                }
                              },
                              "value": {
                                "type": "NumberLiteral",
                                "value": 3,
                                "loc": {
                                  "start": {
                                    "line": 1,
                                    "column": 70
                                  },
                                  "end": {
                                    "line": 1,
                                    "column": 71
                                  }
                                }
                              },
                              "kind": "init",
                              "loc": {
                                "start": {
                                  "line": 1,
                                  "column": 67
                                },
                                "end": {
                                  "line": 1,
                                  "column": 71
                                }
                              }
                            },
                            {
                              "type": "Property",
                              "method": false,
                              "shorthand": false,
                              "computed": false,
                              "key": {
                                "type": "Identifier",
                                "name": "b",
                                "decorators": [],
                                "loc": {
                                  "start": {
                                    "line": 1,
                                    "column": 73
                                  },
                                  "end": {
                                    "line": 1,
                                    "column": 74
                                  }
                                }
                              },
                              "value": {
                                "type": "ObjectExpression",
                                "properties": [
                                  {
                                    "type": "Property",
                                    "method": false,
                                    "shorthand": false,
                                    "computed": false,
                                    "key": {
                                      "type": "Identifier",
                                      "name": "a",
                                      "decorators": [],
                                      "loc": {
                                        "start": {
                                          "line": 1,
                                          "column": 78
                                        },
                                        "end": {
                                          "line": 1,
                                          "column": 79
                                        }
                                      }
                                    },
                                    "value": {
                                      "type": "NumberLiteral",
                                      "value": 2,
                                      "loc": {
                                        "start": {
                                          "line": 1,
                                          "column": 81
                                        },
                                        "end": {
                                          "line": 1,
                                          "column": 82
                                        }
                                      }
                                    },
                                    "kind": "init",
                                    "loc": {
                                      "start": {
                                        "line": 1,
                                        "column": 78
                                      },
                                      "end": {
                                        "line": 1,
                                        "column": 82
                                      }
                                    }
                                  },
                                  {
                                    "type": "Property",
                                    "method": false,
                                    "shorthand": false,
                                    "computed": false,
                                    "key": {
                                      "type": "Identifier",
                                      "name": "b",
                                      "decorators": [],
                                      "loc": {
                                        "start": {
                                          "line": 1,
                                          "column": 84
                                        },
                                        "end": {
                                          "line": 1,
                                          "column": 85
                                        }
                                      }
                                    },
                                    "value": {
                                      "type": "BigIntLiteral",
                                      "value": "",
                                      "bigint": "",
                                      "loc": {
                                        "start": {
                                          "line": 1,
                                          "column": 87
                                        },
                                        "end": {
                                          "line": 1,
                                          "column": 89
                                        }
                                      }
                                    },
                                    "kind": "init",
                                    "loc": {
                                      "start": {
                                        "line": 1,
                                        "column": 84
                                      },
                                      "end": {
                                        "line": 1,
                                        "column": 89
                                      }
                                    }
                                  }
                                ],
                                "loc": {
                                  "start": {
                                    "line": 1,
                                    "column": 76
                                  },
                                  "end": {
                                    "line": 1,
                                    "column": 91
                                  }
                                }
                              },
                              "kind": "init",
                              "loc": {
                                "start": {
                                  "line": 1,
                                  "column": 73
                                },
                                "end": {
                                  "line": 1,
                                  "column": 91
                                }
                              }
                            }
                          ],
                          "loc": {
                            "start": {
                              "line": 1,
                              "column": 65
                            },
                            "end": {
                              "line": 1,
                              "column": 93
                            }
                          }
                        },
                        "loc": {
                          "start": {
                            "line": 1,
                            "column": 61
                          },
                          "end": {
                            "line": 1,
                            "column": 93
                          }
                        }
                      },
                      "kind": "init",
                      "loc": {
                        "start": {
                          "line": 1,
                          "column": 61
                        },
                        "end": {
                          "line": 1,
                          "column": 93
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 22
                    },
                    "end": {
                      "line": 1,
                      "column": 95
                    }
                  }
                }
              ],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 14
                },
                "end": {
                  "line": 1,
                  "column": 96
                }
              }
            },
            "right": {
              "type": "ArrayExpression",
              "elements": [
                {
                  "type": "StringLiteral",
                  "value": "",
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 100
                    },
                    "end": {
                      "line": 1,
                      "column": 105
                    }
                  }
                },
                {
                  "type": "ObjectExpression",
                  "properties": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 107
                    },
                    "end": {
                      "line": 1,
                      "column": 109
                    }
                  }
                },
                {
                  "type": "BigIntLiteral",
                  "value": "",
                  "bigint": "",
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 111
                    },
                    "end": {
                      "line": 1,
                      "column": 113
                    }
                  }
                }
              ],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 99
                },
                "end": {
                  "line": 1,
                  "column": 114
                }
              }
            },
            "loc": {
              "start": {
                "line": 1,
                "column": 14
              },
              "end": {
                "line": 1,
                "column": 114
              }
            }
          },
          {
            "type": "ObjectPattern",
            "properties": [
              {
                "type": "Property",
                "method": false,
                "shorthand": false,
                "computed": false,
                "key": {
                  "type": "Identifier",
                  "name": "r",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 118
                    },
                    "end": {
                      "line": 1,
                      "column": 119
                    }
                  }
                },
                "value": {
                  "type": "AssignmentPattern",
                  "left": {
                    "type": "ArrayPattern",
                    "elements": [
                      {
                        "type": "AssignmentPattern",
                        "left": {
                          "type": "ArrayPattern",
                          "elements": [
                            {
                              "type": "Identifier",
                              "name": "r",
                              "decorators": [],
                              "loc": {
                                "start": {
                                  "line": 1,
                                  "column": 123
                                },
                                "end": {
                                  "line": 1,
                                  "column": 124
                                }
                              }
                            },
                            {
                              "type": "AssignmentPattern",
                              "left": {
                                "type": "Identifier",
                                "name": "z",
                                "decorators": [],
                                "loc": {
                                  "start": {
                                    "line": 1,
                                    "column": 126
                                  },
                                  "end": {
                                    "line": 1,
                                    "column": 127
                                  }
                                }
                              },
                              "right": {
                                "type": "NumberLiteral",
                                "value": 5,
                                "loc": {
                                  "start": {
                                    "line": 1,
                                    "column": 130
                                  },
                                  "end": {
                                    "line": 1,
                                    "column": 131
                                  }
                                }
                              },
                              "loc": {
                                "start": {
                                  "line": 1,
                                  "column": 126
                                },
                                "end": {
                                  "line": 1,
                                  "column": 131
                                }
                              }
                            }
                          ],
                          "loc": {
                            "start": {
                              "line": 1,
                              "column": 122
                            },
                            "end": {
                              "line": 1,
                              "column": 132
                            }
                          }
                        },
                        "right": {
                          "type": "ArrayExpression",
                          "elements": [
                            {
                              "type": "BooleanLiteral",
                              "value": true,
                              "loc": {
                                "start": {
                                  "line": 1,
                                  "column": 136
                                },
                                "end": {
                                  "line": 1,
                                  "column": 140
                                }
                              }
                            }
                          ],
                          "loc": {
                            "start": {
                              "line": 1,
                              "column": 135
                            },
                            "end": {
                              "line": 1,
                              "column": 141
                            }
                          }
                        },
                        "loc": {
                          "start": {
                            "line": 1,
                            "column": 122
                          },
                          "end": {
                            "line": 1,
                            "column": 141
                          }
                        }
                      }
                    ],
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 121
                      },
                      "end": {
                        "line": 1,
                        "column": 142
                      }
                    }
                  },
                  "right": {
                    "type": "ArrayExpression",
                    "elements": [
                      {
                        "type": "ArrayExpression",
                        "elements": [
                          {
                            "type": "NumberLiteral",
                            "value": 2,
                            "loc": {
                              "start": {
                                "line": 1,
                                "column": 147
                              },
                              "end": {
                                "line": 1,
                                "column": 148
                              }
                            }
                          },
                          {
                            "type": "StringLiteral",
                            "value": "",
                            "loc": {
                              "start": {
                                "line": 1,
                                "column": 150
                              },
                              "end": {
                                "line": 1,
                                "column": 155
                              }
                            }
                          }
                        ],
                        "loc": {
                          "start": {
                            "line": 1,
                            "column": 146
                          },
                          "end": {
                            "line": 1,
                            "column": 156
                          }
                        }
                      }
                    ],
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 145
                      },
                      "end": {
                        "line": 1,
                        "column": 157
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 121
                    },
                    "end": {
                      "line": 1,
                      "column": 157
                    }
                  }
                },
                "kind": "init",
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 118
                  },
                  "end": {
                    "line": 1,
                    "column": 157
                  }
                }
              }
            ],
            "loc": {
              "start": {
                "line": 1,
                "column": 116
              },
              "end": {
                "line": 1,
                "column": 159
              }
            }
          }
        ],
        "body": {
          "type": "BlockStatement",
          "statements": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 161
            },
            "end": {
              "line": 3,
              "column": 2
            }
          }
        },
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 3,
            "column": 2
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 3,
          "column": 2
        }
      }
    },
    {
      "type": "ExpressionStatement",
      "expression": {
        "type": "CallExpression",
        "callee": {
          "type": "Identifier",
          "name": "foo",
          "decorators": [],
          "loc": {
            "start": {
              "line": 5,
              "column": 1
            },
            "end": {
              "line": 5,
              "column": 4
            }
          }
        },
        "arguments": [
          {
            "type": "ArrayExpression",
            "elements": [
              {
                "type": "StringLiteral",
                "value": "",
                "loc": {
                  "start": {
                    "line": 5,
                    "column": 6
                  },
                  "end": {
                    "line": 5,
                    "column": 11
                  }
                }
              },
              {
                "type": "ObjectExpression",
                "properties": [],
                "loc": {
                  "start": {
                    "line": 5,
                    "column": 13
                  },
                  "end": {
                    "line": 5,
                    "column": 15
                  }
                }
              },
              {
                "type": "BigIntLiteral",
                "value": "",
                "bigint": "",
                "loc": {
                  "start": {
                    "line": 5,
                    "column": 17
                  },
                  "end": {
                    "line": 5,
                    "column": 19
                  }
                }
              }
            ],
            "loc": {
              "start": {
                "line": 5,
                "column": 5
              },
              "end": {
                "line": 5,
                "column": 20
              }
            }
          },
          {
            "type": "ObjectExpression",
            "properties": [
              {
                "type": "Property",
                "method": false,
                "shorthand": false,
                "computed": false,
                "key": {
                  "type": "Identifier",
                  "name": "r",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 5,
                      "column": 24
                    },
                    "end": {
                      "line": 5,
                      "column": 25
                    }
                  }
                },
                "value": {
                  "type": "ArrayExpression",
                  "elements": [
                    {
                      "type": "ArrayExpression",
                      "elements": [
                        {
                          "type": "StringLiteral",
                          "value": "",
                          "loc": {
                            "start": {
                              "line": 5,
                              "column": 29
                            },
                            "end": {
                              "line": 5,
                              "column": 34
                            }
                          }
                        }
                      ],
                      "loc": {
                        "start": {
                          "line": 5,
                          "column": 28
                        },
                        "end": {
                          "line": 5,
                          "column": 35
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 5,
                      "column": 27
                    },
                    "end": {
                      "line": 5,
                      "column": 36
                    }
                  }
                },
                "kind": "init",
                "loc": {
                  "start": {
                    "line": 5,
                    "column": 24
                  },
                  "end": {
                    "line": 5,
                    "column": 36
                  }
                }
              }
            ],
            "loc": {
              "start": {
                "line": 5,
                "column": 22
              },
              "end": {
                "line": 5,
                "column": 38
              }
            }
          }
        ],
        "optional": false,
        "loc": {
          "start": {
            "line": 5,
            "column": 1
          },
          "end": {
            "line": 5,
            "column": 39
          }
        }
      },
      "loc": {
        "start": {
          "line": 5,
          "column": 1
        },
        "end": {
          "line": 5,
          "column": 40
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 6,
      "column": 1
    }
  }
}
TypeError: Type 'string' is not assignable to type 'number'. [functionWithPattern12.ts:5:29]
