{
  "type": "Program",
  "statements": [
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "ObjectPattern",
            "properties": [
              {
                "type": "Property",
                "method": false,
                "shorthand": false,
                "computed": false,
                "key": {
                  "type": "Identifier",
                  "name": "a",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 7
                    },
                    "end": {
                      "line": 1,
                      "column": 8
                    }
                  }
                },
                "value": {
                  "type": "AssignmentPattern",
                  "left": {
                    "type": "ObjectPattern",
                    "properties": [
                      {
                        "type": "Property",
                        "method": false,
                        "shorthand": true,
                        "computed": false,
                        "key": {
                          "type": "Identifier",
                          "name": "a",
                          "decorators": [],
                          "loc": {
                            "start": {
                              "line": 1,
                              "column": 12
                            },
                            "end": {
                              "line": 1,
                              "column": 13
                            }
                          }
                        },
                        "value": {
                          "type": "AssignmentPattern",
                          "left": {
                            "type": "Identifier",
                            "name": "a",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 1,
                                "column": 12
                              },
                              "end": {
                                "line": 1,
                                "column": 13
                              }
                            }
                          },
                          "right": {
                            "type": "BigIntLiteral",
                            "value": "",
                            "bigint": "",
                            "loc": {
                              "start": {
                                "line": 1,
                                "column": 16
                              },
                              "end": {
                                "line": 1,
                                "column": 18
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 1,
                              "column": 12
                            },
                            "end": {
                              "line": 1,
                              "column": 18
                            }
                          }
                        },
                        "kind": "init",
                        "loc": {
                          "start": {
                            "line": 1,
                            "column": 12
                          },
                          "end": {
                            "line": 1,
                            "column": 18
                          }
                        }
                      },
                      {
                        "type": "Property",
                        "method": false,
                        "shorthand": true,
                        "computed": false,
                        "key": {
                          "type": "Identifier",
                          "name": "b",
                          "decorators": [],
                          "loc": {
                            "start": {
                              "line": 1,
                              "column": 20
                            },
                            "end": {
                              "line": 1,
                              "column": 21
                            }
                          }
                        },
                        "value": {
                          "type": "Identifier",
                          "name": "b",
                          "decorators": [],
                          "loc": {
                            "start": {
                              "line": 1,
                              "column": 20
                            },
                            "end": {
                              "line": 1,
                              "column": 21
                            }
                          }
                        },
                        "kind": "init",
                        "loc": {
                          "start": {
                            "line": 1,
                            "column": 20
                          },
                          "end": {
                            "line": 1,
                            "column": 21
                          }
                        }
                      }
                    ],
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 10
                      },
                      "end": {
                        "line": 1,
                        "column": 23
                      }
                    }
                  },
                  "right": {
                    "type": "ObjectExpression",
                    "properties": [
                      {
                        "type": "Property",
                        "method": false,
                        "shorthand": false,
                        "computed": false,
                        "key": {
                          "type": "Identifier",
                          "name": "b",
                          "decorators": [],
                          "loc": {
                            "start": {
                              "line": 1,
                              "column": 28
                            },
                            "end": {
                              "line": 1,
                              "column": 29
                            }
                          }
                        },
                        "value": {
                          "type": "StringLiteral",
                          "value": "",
                          "loc": {
                            "start": {
                              "line": 1,
                              "column": 31
                            },
                            "end": {
                              "line": 1,
                              "column": 36
                            }
                          }
                        },
                        "kind": "init",
                        "loc": {
                          "start": {
                            "line": 1,
                            "column": 28
                          },
                          "end": {
                            "line": 1,
                            "column": 36
                          }
                        }
                      }
                    ],
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 26
                      },
                      "end": {
                        "line": 1,
                        "column": 38
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 10
                    },
                    "end": {
                      "line": 1,
                      "column": 38
                    }
                  }
                },
                "kind": "init",
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 7
                  },
                  "end": {
                    "line": 1,
                    "column": 38
                  }
                }
              }
            ],
            "loc": {
              "start": {
                "line": 1,
                "column": 5
              },
              "end": {
                "line": 1,
                "column": 40
              }
            }
          },
          "init": {
            "type": "ObjectExpression",
            "properties": [
              {
                "type": "Property",
                "method": false,
                "shorthand": false,
                "computed": false,
                "key": {
                  "type": "Identifier",
                  "name": "a",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 45
                    },
                    "end": {
                      "line": 1,
                      "column": 46
                    }
                  }
                },
                "value": {
                  "type": "ObjectExpression",
                  "properties": [
                    {
                      "type": "Property",
                      "method": false,
                      "shorthand": false,
                      "computed": false,
                      "key": {
                        "type": "Identifier",
                        "name": "a",
                        "decorators": [],
                        "loc": {
                          "start": {
                            "line": 1,
                            "column": 50
                          },
                          "end": {
                            "line": 1,
                            "column": 51
                          }
                        }
                      },
                      "value": {
                        "type": "NumberLiteral",
                        "value": 5,
                        "loc": {
                          "start": {
                            "line": 1,
                            "column": 53
                          },
                          "end": {
                            "line": 1,
                            "column": 54
                          }
                        }
                      },
                      "kind": "init",
                      "loc": {
                        "start": {
                          "line": 1,
                          "column": 50
                        },
                        "end": {
                          "line": 1,
                          "column": 54
                        }
                      }
                    },
                    {
                      "type": "Property",
                      "method": false,
                      "shorthand": false,
                      "computed": false,
                      "key": {
                        "type": "Identifier",
                        "name": "b",
                        "decorators": [],
                        "loc": {
                          "start": {
                            "line": 1,
                            "column": 56
                          },
                          "end": {
                            "line": 1,
                            "column": 57
                          }
                        }
                      },
                      "value": {
                        "type": "BooleanLiteral",
                        "value": true,
                        "loc": {
                          "start": {
                            "line": 1,
                            "column": 59
                          },
                          "end": {
                            "line": 1,
                            "column": 63
                          }
                        }
                      },
                      "kind": "init",
                      "loc": {
                        "start": {
                          "line": 1,
                          "column": 56
                        },
                        "end": {
                          "line": 1,
                          "column": 63
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 48
                    },
                    "end": {
                      "line": 1,
                      "column": 65
                    }
                  }
                },
                "kind": "init",
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 45
                  },
                  "end": {
                    "line": 1,
                    "column": 65
                  }
                }
              }
            ],
            "loc": {
              "start": {
                "line": 1,
                "column": 43
              },
              "end": {
                "line": 1,
                "column": 67
              }
            }
          },
          "loc": {
            "start": {
              "line": 1,
              "column": 5
            },
            "end": {
              "line": 1,
              "column": 67
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 68
        }
      }
    },
    {
      "type": "ExpressionStatement",
      "expression": {
        "type": "AssignmentExpression",
        "operator": "=",
        "left": {
          "type": "Identifier",
          "name": "a",
          "decorators": [],
          "loc": {
            "start": {
              "line": 2,
              "column": 1
            },
            "end": {
              "line": 2,
              "column": 2
            }
          }
        },
        "right": {
          "type": "ObjectExpression",
          "properties": [],
          "loc": {
            "start": {
              "line": 2,
              "column": 5
            },
            "end": {
              "line": 2,
              "column": 7
            }
          }
        },
        "loc": {
          "start": {
            "line": 2,
            "column": 1
          },
          "end": {
            "line": 2,
            "column": 7
          }
        }
      },
      "loc": {
        "start": {
          "line": 2,
          "column": 1
        },
        "end": {
          "line": 2,
          "column": 8
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 2,
      "column": 8
    }
  }
}
TypeError: Type '{ }' is not assignable to type 'number | bigint'. [objectDestructuring12.ts:2:1]
