{
  "type": "Program",
  "statements": [
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "a",
            "typeAnnotation": {
              "type": "TSNumberKeyword",
              "loc": {
                "start": {
                  "line": 1,
                  "column": 7
                },
                "end": {
                  "line": 1,
                  "column": 13
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 5
              },
              "end": {
                "line": 1,
                "column": 6
              }
            }
          },
          "init": null,
          "loc": {
            "start": {
              "line": 1,
              "column": 5
            },
            "end": {
              "line": 1,
              "column": 6
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 14
        }
      }
    },
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "b",
            "typeAnnotation": {
              "type": "TSNumberKeyword",
              "loc": {
                "start": {
                  "line": 2,
                  "column": 8
                },
                "end": {
                  "line": 2,
                  "column": 14
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 2,
                "column": 5
              },
              "end": {
                "line": 2,
                "column": 6
              }
            }
          },
          "init": null,
          "loc": {
            "start": {
              "line": 2,
              "column": 5
            },
            "end": {
              "line": 2,
              "column": 6
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 2,
          "column": 1
        },
        "end": {
          "line": 2,
          "column": 15
        }
      }
    },
    {
      "type": "ExpressionStatement",
      "expression": {
        "type": "AssignmentExpression",
        "operator": "=",
        "left": {
          "type": "ArrayPattern",
          "elements": [
            {
              "type": "ArrayPattern",
              "elements": [
                {
                  "type": "AssignmentPattern",
                  "left": {
                    "type": "Identifier",
                    "name": "a",
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 3,
                        "column": 3
                      },
                      "end": {
                        "line": 3,
                        "column": 4
                      }
                    }
                  },
                  "right": {
                    "type": "NumberLiteral",
                    "value": 5,
                    "loc": {
                      "start": {
                        "line": 3,
                        "column": 7
                      },
                      "end": {
                        "line": 3,
                        "column": 8
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 3,
                      "column": 3
                    },
                    "end": {
                      "line": 3,
                      "column": 8
                    }
                  }
                },
                {
                  "type": "Identifier",
                  "name": "b",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 3,
                      "column": 10
                    },
                    "end": {
                      "line": 3,
                      "column": 11
                    }
                  }
                }
              ],
              "loc": {
                "start": {
                  "line": 3,
                  "column": 2
                },
                "end": {
                  "line": 3,
                  "column": 12
                }
              }
            }
          ],
          "loc": {
            "start": {
              "line": 3,
              "column": 1
            },
            "end": {
              "line": 3,
              "column": 13
            }
          }
        },
        "right": {
          "type": "ArrayExpression",
          "elements": [
            {
              "type": "ArrayExpression",
              "elements": [],
              "loc": {
                "start": {
                  "line": 3,
                  "column": 17
                },
                "end": {
                  "line": 3,
                  "column": 19
                }
              }
            }
          ],
          "loc": {
            "start": {
              "line": 3,
              "column": 16
            },
            "end": {
              "line": 3,
              "column": 20
            }
          }
        },
        "loc": {
          "start": {
            "line": 3,
            "column": 1
          },
          "end": {
            "line": 3,
            "column": 20
          }
        }
      },
      "loc": {
        "start": {
          "line": 3,
          "column": 1
        },
        "end": {
          "line": 3,
          "column": 21
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 3,
      "column": 21
    }
  }
}
TypeError: Initializer provides no value for this binding element and the binding element has no default value [arrayDestructuring39.ts:3:10]
