{
  "type": "Program",
  "statements": [
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "ObjectPattern",
            "properties": [
              {
                "type": "Property",
                "method": false,
                "shorthand": true,
                "computed": false,
                "key": {
                  "type": "Identifier",
                  "name": "a",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 7
                    },
                    "end": {
                      "line": 1,
                      "column": 8
                    }
                  }
                },
                "value": {
                  "type": "AssignmentPattern",
                  "left": {
                    "type": "Identifier",
                    "name": "a",
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 7
                      },
                      "end": {
                        "line": 1,
                        "column": 8
                      }
                    }
                  },
                  "right": {
                    "type": "StringLiteral",
                    "value": "",
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 11
                      },
                      "end": {
                        "line": 1,
                        "column": 16
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 7
                    },
                    "end": {
                      "line": 1,
                      "column": 16
                    }
                  }
                },
                "kind": "init",
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 7
                  },
                  "end": {
                    "line": 1,
                    "column": 16
                  }
                }
              },
              {
                "type": "Property",
                "method": false,
                "shorthand": true,
                "computed": false,
                "key": {
                  "type": "Identifier",
                  "name": "b",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 18
                    },
                    "end": {
                      "line": 1,
                      "column": 19
                    }
                  }
                },
                "value": {
                  "type": "Identifier",
                  "name": "b",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 18
                    },
                    "end": {
                      "line": 1,
                      "column": 19
                    }
                  }
                },
                "kind": "init",
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 18
                  },
                  "end": {
                    "line": 1,
                    "column": 19
                  }
                }
              }
            ],
            "typeAnnotation": {
              "type": "TSTypeLiteral",
              "members": [
                {
                  "type": "TSPropertySignature",
                  "computed": false,
                  "optional": false,
                  "readonly": false,
                  "key": {
                    "type": "Identifier",
                    "name": "a",
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 25
                      },
                      "end": {
                        "line": 1,
                        "column": 26
                      }
                    }
                  },
                  "typeAnnotation": {
                    "type": "TSUnionType",
                    "types": [
                      {
                        "type": "TSNumberKeyword",
                        "loc": {
                          "start": {
                            "line": 1,
                            "column": 28
                          },
                          "end": {
                            "line": 1,
                            "column": 34
                          }
                        }
                      },
                      {
                        "type": "TSStringKeyword",
                        "loc": {
                          "start": {
                            "line": 1,
                            "column": 37
                          },
                          "end": {
                            "line": 1,
                            "column": 43
                          }
                        }
                      }
                    ],
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 28
                      },
                      "end": {
                        "line": 1,
                        "column": 43
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 25
                    },
                    "end": {
                      "line": 1,
                      "column": 44
                    }
                  }
                },
                {
                  "type": "TSPropertySignature",
                  "computed": false,
                  "optional": false,
                  "readonly": false,
                  "key": {
                    "type": "Identifier",
                    "name": "b",
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 45
                      },
                      "end": {
                        "line": 1,
                        "column": 46
                      }
                    }
                  },
                  "typeAnnotation": {
                    "type": "TSNumberKeyword",
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 48
                      },
                      "end": {
                        "line": 1,
                        "column": 54
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 45
                    },
                    "end": {
                      "line": 1,
                      "column": 56
                    }
                  }
                }
              ],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 23
                },
                "end": {
                  "line": 1,
                  "column": 56
                }
              }
            },
            "loc": {
              "start": {
                "line": 1,
                "column": 5
              },
              "end": {
                "line": 1,
                "column": 21
              }
            }
          },
          "init": {
            "type": "ObjectExpression",
            "properties": [
              {
                "type": "Property",
                "method": false,
                "shorthand": false,
                "computed": false,
                "key": {
                  "type": "Identifier",
                  "name": "a",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 61
                    },
                    "end": {
                      "line": 1,
                      "column": 62
                    }
                  }
                },
                "value": {
                  "type": "NumberLiteral",
                  "value": 12,
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 64
                    },
                    "end": {
                      "line": 1,
                      "column": 66
                    }
                  }
                },
                "kind": "init",
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 61
                  },
                  "end": {
                    "line": 1,
                    "column": 66
                  }
                }
              },
              {
                "type": "Property",
                "method": false,
                "shorthand": false,
                "computed": false,
                "key": {
                  "type": "Identifier",
                  "name": "b",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 68
                    },
                    "end": {
                      "line": 1,
                      "column": 69
                    }
                  }
                },
                "value": {
                  "type": "BooleanLiteral",
                  "value": true,
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 71
                    },
                    "end": {
                      "line": 1,
                      "column": 75
                    }
                  }
                },
                "kind": "init",
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 68
                  },
                  "end": {
                    "line": 1,
                    "column": 75
                  }
                }
              }
            ],
            "loc": {
              "start": {
                "line": 1,
                "column": 59
              },
              "end": {
                "line": 1,
                "column": 77
              }
            }
          },
          "loc": {
            "start": {
              "line": 1,
              "column": 5
            },
            "end": {
              "line": 1,
              "column": 77
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 78
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 1,
      "column": 78
    }
  }
}
TypeError: Type 'boolean' is not assignable to type 'number'. [objectDestructuring17.ts:1:68]
