{
  "type": "Program",
  "statements": [
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "a",
            "typeAnnotation": {
              "type": "TSTypeLiteral",
              "members": [
                {
                  "type": "TSPropertySignature",
                  "computed": false,
                  "optional": false,
                  "readonly": false,
                  "key": {
                    "type": "Identifier",
                    "name": "b",
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 10
                      },
                      "end": {
                        "line": 1,
                        "column": 11
                      }
                    }
                  },
                  "typeAnnotation": {
                    "type": "TSNumberKeyword",
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 13
                      },
                      "end": {
                        "line": 1,
                        "column": 19
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 10
                    },
                    "end": {
                      "line": 1,
                      "column": 21
                    }
                  }
                }
              ],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 8
                },
                "end": {
                  "line": 1,
                  "column": 21
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 5
              },
              "end": {
                "line": 1,
                "column": 6
              }
            }
          },
          "init": {
            "type": "ObjectExpression",
            "properties": [
              {
                "type": "Property",
                "method": false,
                "shorthand": false,
                "computed": false,
                "key": {
                  "type": "Identifier",
                  "name": "b",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 30
                    },
                    "end": {
                      "line": 1,
                      "column": 31
                    }
                  }
                },
                "value": {
                  "type": "FunctionExpression",
                  "function": {
                    "type": "ScriptFunction",
                    "id": null,
                    "generator": false,
                    "async": false,
                    "expression": false,
                    "params": [],
                    "returnType": {
                      "type": "TSStringKeyword",
                      "loc": {
                        "start": {
                          "line": 1,
                          "column": 35
                        },
                        "end": {
                          "line": 1,
                          "column": 41
                        }
                      }
                    },
                    "body": {
                      "type": "BlockStatement",
                      "statements": [
                        {
                          "type": "ReturnStatement",
                          "argument": {
                            "type": "StringLiteral",
                            "value": "",
                            "loc": {
                              "start": {
                                "line": 1,
                                "column": 51
                              },
                              "end": {
                                "line": 1,
                                "column": 56
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 1,
                              "column": 44
                            },
                            "end": {
                              "line": 1,
                              "column": 56
                            }
                          }
                        }
                      ],
                      "loc": {
                        "start": {
                          "line": 1,
                          "column": 42
                        },
                        "end": {
                          "line": 1,
                          "column": 58
                        }
                      }
                    },
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 31
                      },
                      "end": {
                        "line": 1,
                        "column": 58
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 31
                    },
                    "end": {
                      "line": 1,
                      "column": 58
                    }
                  }
                },
                "kind": "get",
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 26
                  },
                  "end": {
                    "line": 1,
                    "column": 58
                  }
                }
              }
            ],
            "loc": {
              "start": {
                "line": 1,
                "column": 24
              },
              "end": {
                "line": 1,
                "column": 60
              }
            }
          },
          "loc": {
            "start": {
              "line": 1,
              "column": 5
            },
            "end": {
              "line": 1,
              "column": 60
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 60
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 1,
      "column": 60
    }
  }
}
TypeError: Type 'string' is not assignable to type 'number'. [objectLiteralAssignability4.ts:1:26]
