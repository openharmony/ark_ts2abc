{
  "type": "Program",
  "statements": [
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "a",
            "typeAnnotation": {
              "type": "TSNumberKeyword",
              "loc": {
                "start": {
                  "line": 2,
                  "column": 8
                },
                "end": {
                  "line": 2,
                  "column": 14
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 2,
                "column": 5
              },
              "end": {
                "line": 2,
                "column": 6
              }
            }
          },
          "init": null,
          "loc": {
            "start": {
              "line": 2,
              "column": 5
            },
            "end": {
              "line": 2,
              "column": 6
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 2,
          "column": 1
        },
        "end": {
          "line": 2,
          "column": 15
        }
      }
    },
    {
      "type": "ExpressionStatement",
      "expression": {
        "type": "AssignmentExpression",
        "operator": "=",
        "left": {
          "type": "ObjectPattern",
          "properties": [
            {
              "type": "Property",
              "method": false,
              "shorthand": true,
              "computed": false,
              "key": {
                "type": "Identifier",
                "name": "a",
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 3,
                    "column": 3
                  },
                  "end": {
                    "line": 3,
                    "column": 4
                  }
                }
              },
              "value": {
                "type": "Identifier",
                "name": "a",
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 3,
                    "column": 3
                  },
                  "end": {
                    "line": 3,
                    "column": 4
                  }
                }
              },
              "kind": "init",
              "loc": {
                "start": {
                  "line": 3,
                  "column": 3
                },
                "end": {
                  "line": 3,
                  "column": 4
                }
              }
            }
          ],
          "loc": {
            "start": {
              "line": 3,
              "column": 2
            },
            "end": {
              "line": 3,
              "column": 5
            }
          }
        },
        "right": {
          "type": "ObjectExpression",
          "properties": [
            {
              "type": "Property",
              "method": false,
              "shorthand": false,
              "computed": false,
              "key": {
                "type": "Identifier",
                "name": "a",
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 3,
                    "column": 9
                  },
                  "end": {
                    "line": 3,
                    "column": 10
                  }
                }
              },
              "value": {
                "type": "NumberLiteral",
                "value": 3,
                "loc": {
                  "start": {
                    "line": 3,
                    "column": 13
                  },
                  "end": {
                    "line": 3,
                    "column": 14
                  }
                }
              },
              "kind": "init",
              "loc": {
                "start": {
                  "line": 3,
                  "column": 9
                },
                "end": {
                  "line": 3,
                  "column": 14
                }
              }
            },
            {
              "type": "Property",
              "method": false,
              "shorthand": false,
              "computed": false,
              "key": {
                "type": "Identifier",
                "name": "a",
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 3,
                    "column": 16
                  },
                  "end": {
                    "line": 3,
                    "column": 17
                  }
                }
              },
              "value": {
                "type": "NumberLiteral",
                "value": 4,
                "loc": {
                  "start": {
                    "line": 3,
                    "column": 20
                  },
                  "end": {
                    "line": 3,
                    "column": 21
                  }
                }
              },
              "kind": "init",
              "loc": {
                "start": {
                  "line": 3,
                  "column": 16
                },
                "end": {
                  "line": 3,
                  "column": 21
                }
              }
            }
          ],
          "loc": {
            "start": {
              "line": 3,
              "column": 8
            },
            "end": {
              "line": 3,
              "column": 22
            }
          }
        },
        "loc": {
          "start": {
            "line": 3,
            "column": 1
          },
          "end": {
            "line": 3,
            "column": 23
          }
        }
      },
      "loc": {
        "start": {
          "line": 3,
          "column": 1
        },
        "end": {
          "line": 3,
          "column": 23
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 5,
      "column": 1
    }
  }
}
TypeError: Duplicate identifier 'a'. [objectDestructuring33.ts:3:16]
