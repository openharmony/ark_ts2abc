{
  "type": "Program",
  "statements": [
    {
      "type": "TSDeclareFunction",
      "function": {
        "type": "ScriptFunction",
        "id": {
          "type": "Identifier",
          "name": "foo",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 10
            },
            "end": {
              "line": 1,
              "column": 13
            }
          }
        },
        "generator": false,
        "async": false,
        "expression": false,
        "params": [
          {
            "type": "Identifier",
            "name": "a",
            "typeAnnotation": {
              "type": "TSTypeLiteral",
              "members": [
                {
                  "type": "TSPropertySignature",
                  "computed": false,
                  "optional": false,
                  "readonly": false,
                  "key": {
                    "type": "Identifier",
                    "name": "x",
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 19
                      },
                      "end": {
                        "line": 1,
                        "column": 20
                      }
                    }
                  },
                  "typeAnnotation": {
                    "type": "TSNumberKeyword",
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 22
                      },
                      "end": {
                        "line": 1,
                        "column": 28
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 19
                    },
                    "end": {
                      "line": 1,
                      "column": 29
                    }
                  }
                },
                {
                  "type": "TSPropertySignature",
                  "computed": false,
                  "optional": false,
                  "readonly": false,
                  "key": {
                    "type": "Identifier",
                    "name": "y",
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 30
                      },
                      "end": {
                        "line": 1,
                        "column": 31
                      }
                    }
                  },
                  "typeAnnotation": {
                    "type": "TSStringKeyword",
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 33
                      },
                      "end": {
                        "line": 1,
                        "column": 39
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 30
                    },
                    "end": {
                      "line": 1,
                      "column": 41
                    }
                  }
                }
              ],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 17
                },
                "end": {
                  "line": 1,
                  "column": 41
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 14
              },
              "end": {
                "line": 1,
                "column": 15
              }
            }
          }
        ],
        "returnType": {
          "type": "TSNumberKeyword",
          "loc": {
            "start": {
              "line": 1,
              "column": 44
            },
            "end": {
              "line": 1,
              "column": 50
            }
          }
        },
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 51
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 51
        }
      }
    },
    {
      "type": "TSDeclareFunction",
      "function": {
        "type": "ScriptFunction",
        "id": {
          "type": "Identifier",
          "name": "foo",
          "decorators": [],
          "loc": {
            "start": {
              "line": 2,
              "column": 10
            },
            "end": {
              "line": 2,
              "column": 13
            }
          }
        },
        "generator": false,
        "async": false,
        "expression": false,
        "params": [
          {
            "type": "Identifier",
            "name": "a",
            "typeAnnotation": {
              "type": "TSTypeLiteral",
              "members": [
                {
                  "type": "TSPropertySignature",
                  "computed": false,
                  "optional": false,
                  "readonly": false,
                  "key": {
                    "type": "Identifier",
                    "name": "x",
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 2,
                        "column": 19
                      },
                      "end": {
                        "line": 2,
                        "column": 20
                      }
                    }
                  },
                  "typeAnnotation": {
                    "type": "TSStringKeyword",
                    "loc": {
                      "start": {
                        "line": 2,
                        "column": 22
                      },
                      "end": {
                        "line": 2,
                        "column": 28
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 19
                    },
                    "end": {
                      "line": 2,
                      "column": 29
                    }
                  }
                },
                {
                  "type": "TSPropertySignature",
                  "computed": false,
                  "optional": false,
                  "readonly": false,
                  "key": {
                    "type": "Identifier",
                    "name": "y",
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 2,
                        "column": 30
                      },
                      "end": {
                        "line": 2,
                        "column": 31
                      }
                    }
                  },
                  "typeAnnotation": {
                    "type": "TSNumberKeyword",
                    "loc": {
                      "start": {
                        "line": 2,
                        "column": 33
                      },
                      "end": {
                        "line": 2,
                        "column": 39
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 30
                    },
                    "end": {
                      "line": 2,
                      "column": 41
                    }
                  }
                }
              ],
              "loc": {
                "start": {
                  "line": 2,
                  "column": 17
                },
                "end": {
                  "line": 2,
                  "column": 41
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 2,
                "column": 14
              },
              "end": {
                "line": 2,
                "column": 15
              }
            }
          }
        ],
        "returnType": {
          "type": "TSNumberKeyword",
          "loc": {
            "start": {
              "line": 2,
              "column": 44
            },
            "end": {
              "line": 2,
              "column": 50
            }
          }
        },
        "loc": {
          "start": {
            "line": 2,
            "column": 1
          },
          "end": {
            "line": 2,
            "column": 51
          }
        }
      },
      "loc": {
        "start": {
          "line": 2,
          "column": 1
        },
        "end": {
          "line": 2,
          "column": 51
        }
      }
    },
    {
      "type": "FunctionDeclaration",
      "function": {
        "type": "ScriptFunction",
        "id": {
          "type": "Identifier",
          "name": "foo",
          "decorators": [],
          "loc": {
            "start": {
              "line": 3,
              "column": 10
            },
            "end": {
              "line": 3,
              "column": 13
            }
          }
        },
        "generator": false,
        "async": false,
        "expression": false,
        "params": [
          {
            "type": "Identifier",
            "name": "a",
            "typeAnnotation": {
              "type": "TSTypeLiteral",
              "members": [
                {
                  "type": "TSPropertySignature",
                  "computed": false,
                  "optional": false,
                  "readonly": false,
                  "key": {
                    "type": "Identifier",
                    "name": "x",
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 3,
                        "column": 19
                      },
                      "end": {
                        "line": 3,
                        "column": 20
                      }
                    }
                  },
                  "typeAnnotation": {
                    "type": "TSUnionType",
                    "types": [
                      {
                        "type": "TSNumberKeyword",
                        "loc": {
                          "start": {
                            "line": 3,
                            "column": 22
                          },
                          "end": {
                            "line": 3,
                            "column": 28
                          }
                        }
                      },
                      {
                        "type": "TSStringKeyword",
                        "loc": {
                          "start": {
                            "line": 3,
                            "column": 31
                          },
                          "end": {
                            "line": 3,
                            "column": 37
                          }
                        }
                      }
                    ],
                    "loc": {
                      "start": {
                        "line": 3,
                        "column": 22
                      },
                      "end": {
                        "line": 3,
                        "column": 37
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 3,
                      "column": 19
                    },
                    "end": {
                      "line": 3,
                      "column": 39
                    }
                  }
                }
              ],
              "loc": {
                "start": {
                  "line": 3,
                  "column": 17
                },
                "end": {
                  "line": 3,
                  "column": 39
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 3,
                "column": 14
              },
              "end": {
                "line": 3,
                "column": 15
              }
            }
          }
        ],
        "body": {
          "type": "BlockStatement",
          "statements": [
            {
              "type": "ReturnStatement",
              "argument": {
                "type": "NumberLiteral",
                "value": 2,
                "loc": {
                  "start": {
                    "line": 4,
                    "column": 12
                  },
                  "end": {
                    "line": 4,
                    "column": 13
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 4,
                  "column": 5
                },
                "end": {
                  "line": 4,
                  "column": 14
                }
              }
            }
          ],
          "loc": {
            "start": {
              "line": 3,
              "column": 41
            },
            "end": {
              "line": 5,
              "column": 2
            }
          }
        },
        "loc": {
          "start": {
            "line": 3,
            "column": 1
          },
          "end": {
            "line": 5,
            "column": 2
          }
        }
      },
      "loc": {
        "start": {
          "line": 3,
          "column": 1
        },
        "end": {
          "line": 5,
          "column": 2
        }
      }
    },
    {
      "type": "ExpressionStatement",
      "expression": {
        "type": "CallExpression",
        "callee": {
          "type": "Identifier",
          "name": "foo",
          "decorators": [],
          "loc": {
            "start": {
              "line": 7,
              "column": 1
            },
            "end": {
              "line": 7,
              "column": 4
            }
          }
        },
        "arguments": [
          {
            "type": "ObjectExpression",
            "properties": [
              {
                "type": "Property",
                "method": false,
                "shorthand": false,
                "computed": false,
                "key": {
                  "type": "Identifier",
                  "name": "x",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 7,
                      "column": 7
                    },
                    "end": {
                      "line": 7,
                      "column": 8
                    }
                  }
                },
                "value": {
                  "type": "BooleanLiteral",
                  "value": true,
                  "loc": {
                    "start": {
                      "line": 7,
                      "column": 10
                    },
                    "end": {
                      "line": 7,
                      "column": 14
                    }
                  }
                },
                "kind": "init",
                "loc": {
                  "start": {
                    "line": 7,
                    "column": 7
                  },
                  "end": {
                    "line": 7,
                    "column": 14
                  }
                }
              }
            ],
            "loc": {
              "start": {
                "line": 7,
                "column": 5
              },
              "end": {
                "line": 7,
                "column": 16
              }
            }
          }
        ],
        "optional": false,
        "loc": {
          "start": {
            "line": 7,
            "column": 1
          },
          "end": {
            "line": 7,
            "column": 17
          }
        }
      },
      "loc": {
        "start": {
          "line": 7,
          "column": 1
        },
        "end": {
          "line": 7,
          "column": 18
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 7,
      "column": 18
    }
  }
}
TypeError: No overload matches this call. [functionOverload1.ts:7:1]
